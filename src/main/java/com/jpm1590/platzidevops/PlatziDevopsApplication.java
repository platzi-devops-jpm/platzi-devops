package com.jpm1590.platzidevops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlatziDevopsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlatziDevopsApplication.class, args);
	}

}
